
import java.sql.Connection;
import java.sql.DriverManager;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author ronal
 */
public class Conexion {
     public static final String URL = "jdbc:mysql://localhost:3306/examenfinalprogra2B-2";
    public static final String user = "root";
    public static final String pass = "123456";

    public static Connection getConnection() {
        Connection conexion = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conexion = DriverManager.getConnection(URL, user, pass);
        } catch (Exception e) {
            System.out.println("ERROR," + e);
        }
        return conexion;
    }
}
